package com.example.eclipse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.example.eclipse.adapter.TrailerAdapter;
import com.example.eclipse.clients.GetMovieTrailerService;
import com.example.eclipse.listeners.TrailerClickListener;
import com.example.eclipse.models.Movie;
import com.example.eclipse.models.MovieTrailer;
import com.example.eclipse.utils.RetrofitClientInstance;
import com.example.eclipse.wrappers.MovieTrailerWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrailerActivity extends AppCompatActivity {

    private List<MovieTrailer> movieTrailers;

    private TextView txtTrailer;
    private CardView cvTrailer;
    private RecyclerView recyclerTrailer;

    private Movie movie;

    private TrailerAdapter trailerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trailer);

        txtTrailer = findViewById(R.id.txt_MovieTrailer);
        cvTrailer = findViewById(R.id.cv_movie_trailer);
        recyclerTrailer = findViewById(R.id.recycler_trailer);

        recyclerTrailer.setLayoutManager(new LinearLayoutManager(this));
        recyclerTrailer.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerTrailer.setNestedScrollingEnabled(false);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                movie = (Movie) bundle.getSerializable("movie");
                if (movie != null) {
                    getTrailerForMovie(movie.getId());
                }
            }
        }
    }

    private void populateTrailerForMovie(List<MovieTrailer> trailers) {
        if (trailers.size() > 0) {
            trailerAdapter = new TrailerAdapter(trailers, new TrailerClickListener() {
                @Override
                public void onTrailerClick(MovieTrailer movieTrailer) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=" + movieTrailer.getKey())));
                }
            });
            recyclerTrailer.setAdapter(trailerAdapter);
        }
    }

    private void getTrailerForMovie(final int id) {
        GetMovieTrailerService movieTrailerService = RetrofitClientInstance
                .getRetrofitInstance().create(GetMovieTrailerService.class);
        Call<MovieTrailerWrapper> call = movieTrailerService.getTrailers(id);

        call.enqueue(new Callback<MovieTrailerWrapper>() {
            @Override
            public void onResponse(Call<MovieTrailerWrapper> call, Response<MovieTrailerWrapper> response) {
                if (response.body() != null) {
                    movieTrailers = response.body().getTrailerResult();
                    populateTrailerForMovie(movieTrailers);
                }
            }

            @Override
            public void onFailure(Call<MovieTrailerWrapper> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("movie", movie);
    }
}