package com.example.eclipse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eclipse.adapter.TrailerAdapter;
import com.example.eclipse.clients.GetCastDataService;
import com.example.eclipse.clients.GetMovieTrailerService;
import com.example.eclipse.listeners.TrailerClickListener;
import com.example.eclipse.models.Actor;
import com.example.eclipse.models.Cast;
import com.example.eclipse.models.MovieTrailer;
import com.example.eclipse.utils.RetrofitClientInstance;
import com.example.eclipse.wrappers.ActorWrapper;
import com.example.eclipse.wrappers.MovieTrailerWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieActorActivity extends AppCompatActivity {

    private ImageView imgProfile;
    private TextView txtName, txtBirthday, txtBio;

    private String profilePic, profileName, profileBirthday, profileDeath, profileBio, profileImdB;

    private Cast cast;
    private List<Actor> actorList;

    private AlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_actor);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        txtName = findViewById(R.id.txtActorName);
        txtBirthday = findViewById(R.id.txtBirthActor);
        txtBio = findViewById(R.id.txtActorBio);
        imgProfile = findViewById(R.id.imageCast_Bio);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                cast = (Cast) bundle.getSerializable("cast");
                if (cast != null) {
                    getActorForMovie(cast.getId());
                }
            }
        }
    }

    private void getActorForMovie(final int id) {
        GetCastDataService actorService = RetrofitClientInstance
                .getRetrofitInstance().create(GetCastDataService.class);
        Call<Actor> call = actorService.getActor(id);

        call.enqueue(new Callback<Actor>() {
            @Override
            public void onResponse(Call<Actor> call, final Response<Actor> response) {
                if (response.body() != null) {
                    txtName.setText(response.body().getName());
                    txtBio.setText(response.body().getBio());

                    profileBirthday = response.body().getBirthDay();
                    profileDeath = response.body().getDeathDay();

                    if (profileDeath != null) {
                        String actorDay = profileBirthday + " / " + profileDeath;
                        txtBirthday.setText(actorDay);
                    }
                    else {
                        txtBirthday.setText(profileBirthday);
                    }

                    profilePic = "https://image.tmdb.org/t/p/w342" + response.body().getImage();
                    Picasso.get()
                            .load(profilePic)
                            .placeholder(R.drawable.ic_baseline_cached_24)
                            .error(R.drawable.ic_baseline_block_24)
                            .into(imgProfile);

                    imgProfile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.imdb.com/name/" + response.body().getImdb())));
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<Actor> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("cast", cast);;
    }
}