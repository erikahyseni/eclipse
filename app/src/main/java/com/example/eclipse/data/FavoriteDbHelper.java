package com.example.eclipse.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import androidx.annotation.Nullable;

import com.example.eclipse.models.Movie;

import java.util.ArrayList;
import java.util.List;

public class FavoriteDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "eclipse.db";
    private static final int DATABASE_VERSION = 1;

    SQLiteOpenHelper dbHandler;
    SQLiteDatabase database;

    public FavoriteDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        database = dbHandler.getWritableDatabase();
    }

    public void close() {
        dbHandler.close();
    }

    //String Databaza
    private static final String CREATE_FINAL_MOVIE_DB = "CREATE TABLE " + FavoriteContract.FavoriteEntry.TABLE_NAME +
            " (" + FavoriteContract.FavoriteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            FavoriteContract.FavoriteEntry.COLUMN_ID + " INTEGER, " +
            FavoriteContract.FavoriteEntry.COLUMN_TITLE + " TEXT NOT NULL, " +
            FavoriteContract.FavoriteEntry.COLUMN_VOTE_AVERAGE + " REAL NOT NULL, " +
            FavoriteContract.FavoriteEntry.COLUMN_POSTER + " TEXT NOT NULL, " +
            FavoriteContract.FavoriteEntry.COLUMN_COVER + " TEXT NOT NULL, " +
            FavoriteContract.FavoriteEntry.COLUMN_OVERVIEW + " TEXT NOT NULL, " +
            FavoriteContract.FavoriteEntry.COLUMN_RELEASE_DATE + " TEXT NOT NULL " + "); ";

    //Krijimi
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FINAL_MOVIE_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FavoriteContract.FavoriteEntry.TABLE_NAME);
        onCreate(db);
    }

    //Ruan favorites
    public void addFavorite(Movie movie) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        ContentValues values = new ContentValues();
        values.put(FavoriteContract.FavoriteEntry.COLUMN_ID, movie.getId());
        values.put(FavoriteContract.FavoriteEntry.COLUMN_TITLE, movie.getTitle());
        values.put(FavoriteContract.FavoriteEntry.COLUMN_VOTE_AVERAGE, movie.getVoteAverage());
        values.put(FavoriteContract.FavoriteEntry.COLUMN_POSTER, movie.getThumbnail());
        values.put(FavoriteContract.FavoriteEntry.COLUMN_COVER, movie.getCover_image());
        values.put(FavoriteContract.FavoriteEntry.COLUMN_OVERVIEW, movie.getDescription());
        values.put(FavoriteContract.FavoriteEntry.COLUMN_RELEASE_DATE, movie.getReleaseDate());

        sqLiteDatabase.insert(FavoriteContract.FavoriteEntry.TABLE_NAME, null, values);
        sqLiteDatabase.close();
    }

    //Fshirja
    public void deleteFavorite(int id) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.delete(FavoriteContract.FavoriteEntry.TABLE_NAME,
                FavoriteContract.FavoriteEntry.COLUMN_ID + "=" + id, null);
    }

    //Listimi
    public List<Movie> getMovies() {
        List<Movie> movieList = new ArrayList<>();

        SQLiteDatabase sql = this.getReadableDatabase();

        String query = "SELECT * FROM " + FavoriteContract.FavoriteEntry.TABLE_NAME;

        Cursor cursor = sql.rawQuery(query, null);
        initializeCursor(movieList, cursor);
        return movieList;
    }

    private void initializeCursor(List<Movie> movieList, Cursor cursor) {
        if (cursor.moveToFirst()) {
            do {
                Movie movie = new Movie();
                movie.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_ID))));
                movie.setTitle(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_TITLE)));
                movie.setVoteAverage(Double.parseDouble(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_VOTE_AVERAGE))));
                movie.setThumbnail(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_POSTER)));
                movie.setCover_image(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_COVER)));
                movie.setDescription(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_OVERVIEW)));
                movie.setReleaseDate(cursor.getString(cursor.getColumnIndex(FavoriteContract.FavoriteEntry.COLUMN_RELEASE_DATE)));

                movieList.add(movie);
            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    //A ekziston
    public boolean checkIfMovieExist(int movieId) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        String query = "SELECT * FROM " + FavoriteContract.FavoriteEntry.TABLE_NAME +
                " WHERE " + FavoriteContract.FavoriteEntry.COLUMN_ID + "=" + movieId;

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    //Kerkon
    public List<Movie> searchMovie(String searchMovie) {
        List<Movie> movieList = new ArrayList<>();

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        String query = "SELECT * FROM " + FavoriteContract.FavoriteEntry.TABLE_NAME +
                " WHERE " + FavoriteContract.FavoriteEntry.COLUMN_TITLE + " LIKE '" + searchMovie + "%'";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        initializeCursor(movieList, cursor);
        sqLiteDatabase.close();
        return movieList;
    }
}

class FavoriteContract {
    public static final class FavoriteEntry implements BaseColumns {
        public static final String TABLE_NAME = "favorites";
        public static final String COLUMN_ID = "movieId";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_VOTE_AVERAGE = "vote_average";
        public static final String COLUMN_POSTER = "poster";
        public static final String COLUMN_COVER = "cover";
        public static final String COLUMN_OVERVIEW = "overview";
        public static final String COLUMN_RELEASE_DATE = "release_date";
    }
}
