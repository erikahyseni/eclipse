package com.example.eclipse.listeners;

import com.example.eclipse.models.MovieTrailer;

public interface TrailerClickListener {
    void onTrailerClick(MovieTrailer movieTrailer);
}
