package com.example.eclipse.listeners;

import com.example.eclipse.models.Movie;

public interface MovieClickListener {
    void onMovieClick(Movie movie);
}
