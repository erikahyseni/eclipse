package com.example.eclipse.listeners;

import com.example.eclipse.models.Cast;

public interface CastClickListener {
    void onCastClick(Cast cast);
}
