package com.example.eclipse.wrappers;

import com.example.eclipse.models.Genre;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GenreWrapper {
    @SerializedName("genres")
    private ArrayList<Genre> genres;

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }
}
