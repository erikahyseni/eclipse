package com.example.eclipse.wrappers;

import com.example.eclipse.models.Actor;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ActorWrapper {

    @SerializedName("object")
    private ArrayList<Actor> actorsList;

    public ArrayList<Actor> getActorsList() {
        return actorsList;
    }

    public void setActorsList(ArrayList<Actor> actorsList) {
        this.actorsList = actorsList;
    }
}
