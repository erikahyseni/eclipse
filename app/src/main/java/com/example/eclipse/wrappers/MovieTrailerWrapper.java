package com.example.eclipse.wrappers;

import com.example.eclipse.models.MovieTrailer;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieTrailerWrapper {
    @SerializedName("id")
    private int id;
    @SerializedName("results")
    private ArrayList<MovieTrailer> trailerResult;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<MovieTrailer> getTrailerResult() {
        return trailerResult;
    }

    public void setTrailerResult(ArrayList<MovieTrailer> trailerResult) {
        this.trailerResult = trailerResult;
    }
}
