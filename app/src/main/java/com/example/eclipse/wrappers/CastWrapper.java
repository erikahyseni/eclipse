package com.example.eclipse.wrappers;

import com.example.eclipse.models.Cast;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CastWrapper {
    @SerializedName("id")
    private int id;
    @SerializedName("cast")
    private ArrayList<Cast> castList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Cast> getCastList() {
        return castList;
    }

    public void setCastList(ArrayList<Cast> castList) {
        this.castList = castList;
    }
}
