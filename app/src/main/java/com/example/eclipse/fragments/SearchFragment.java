package com.example.eclipse.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.MovieDetailActivity;
import com.example.eclipse.R;
import com.example.eclipse.adapter.SearchAdapter;
import com.example.eclipse.clients.GetGenreDataService;
import com.example.eclipse.clients.GetMovieDataService;
import com.example.eclipse.listeners.MovieClickListener;
import com.example.eclipse.models.Genre;
import com.example.eclipse.models.Movie;
import com.example.eclipse.utils.RetrofitClientInstance;
import com.example.eclipse.wrappers.GenreWrapper;
import com.example.eclipse.wrappers.MovieWrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment implements SearchView.OnQueryTextListener {

    private RecyclerView searchMovieRecycler;
    private SearchAdapter searchAdapter;
    private List<Genre> genreList = new ArrayList<>();

    private SearchView searchView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        searchView = view.findViewById(R.id.search_movie);
        searchView.setOnQueryTextListener(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchMovies();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void populateMovies(List<Movie> movieList) {
        if (movieList.size() > 0) {
            searchAdapter = new SearchAdapter(movieList, new MovieClickListener() {
                @Override
                public void onMovieClick(Movie movie) {
                    Movie.vibrateOnClick(getContext(), (short) 50);
                    Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("movie", movie);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }
    }

    private void getMovie(final RecyclerView recyclerView, Call<MovieWrapper> call) {
        try {
            call.enqueue(new Callback<MovieWrapper>() {
                @Override
                public void onResponse(Call<MovieWrapper> call, Response<MovieWrapper> response) {
                    if (response.body() != null) {
                        List<Movie> movieList = response.body().getMovieList();

                        for (Movie m : movieList) {
                            m.mapGenres(genreList);
                        }

                        populateMovies(movieList);

                        recyclerView.setAdapter(searchAdapter);
                        recyclerView.smoothScrollToPosition(0);
                    }
                }

                @Override
                public void onFailure(Call<MovieWrapper> call, Throwable t) {

                }
            });
        }
        catch (Exception e) {

        }
    }

    private void searchMovies() {
        searchMovieRecycler = getActivity().findViewById(R.id.movie_search_recycler);
        searchMovieRecycler.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        searchMovieRecycler.setItemAnimator(new DefaultItemAnimator());

        getMovieGenre();

        String movie = searchView.getQuery().toString();

        GetMovieDataService searchService = RetrofitClientInstance.getRetrofitInstance().create(GetMovieDataService.class);
        Call<MovieWrapper> call = searchService.getMovie(movie);

        getMovie(searchMovieRecycler, call);
    }

    private void getMovieGenre() {
        try {
            GetGenreDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetGenreDataService.class);
            Call<GenreWrapper> call = service.getGenre();
            call.enqueue(new Callback<GenreWrapper>() {
                @Override
                public void onResponse(Call<GenreWrapper> call, Response<GenreWrapper> response) {
                    if (response.body() != null) {
                        genreList = response.body().getGenres();
                    }
                }

                @Override
                public void onFailure(Call<GenreWrapper> call, Throwable t) {

                }
            });
        }
        catch (Exception e) {

        }
    }
}
