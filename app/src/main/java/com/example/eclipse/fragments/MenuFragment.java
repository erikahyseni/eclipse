package com.example.eclipse.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.MovieDetailActivity;
import com.example.eclipse.R;
import com.example.eclipse.adapter.MovieAdapter;
import com.example.eclipse.adapter.MovieMenuAdapter;
import com.example.eclipse.clients.GetGenreDataService;
import com.example.eclipse.clients.GetMovieDataService;
import com.example.eclipse.data.FavoriteDbHelper;
import com.example.eclipse.listeners.MovieClickListener;
import com.example.eclipse.models.Genre;
import com.example.eclipse.models.Movie;
import com.example.eclipse.utils.RetrofitClientInstance;
import com.example.eclipse.wrappers.GenreWrapper;
import com.example.eclipse.wrappers.MovieWrapper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuFragment extends Fragment {

    private List<Genre> genreList = new ArrayList<>();

    private RecyclerView recyclerMovies, recyclerPopular, recyclerTopRated, recyclerUpcoming;

    private MovieAdapter movieAdapter;
    private MovieMenuAdapter movieMenuAdapter;

    private AlertDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (isNetworkAvailable()) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading...");
            progressDialog.show();

            GetMovieDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetMovieDataService.class);

            getMovieGenres();

            initViewHorizontal(view, recyclerMovies, R.id.recycler_movies, service.movieNowPlaying());

            initViewVertical(view, recyclerPopular, R.id.recycler_popular, service.moviePopular());
            initViewVertical(view, recyclerTopRated, R.id.recycler_top_rated, service.movieTopRated());
            initViewVertical(view, recyclerUpcoming, R.id.recycler_upcoming, service.movieUpcoming());
        }
    }

    private void initRecyclerViewHorizontal(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initRecyclerViewVertical(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setNestedScrollingEnabled(true);
    }

    private void initViewHorizontal(View view, RecyclerView recyclerView, int recycler, Call<MovieWrapper> call) {
        recyclerView = view.findViewById(recycler);

        initRecyclerViewHorizontal(recyclerView);

        getMovieHorizontal(recyclerView, call);
    }

    private void initViewVertical(View view, RecyclerView recyclerView, int recycler, Call<MovieWrapper> call) {
        recyclerView = view.findViewById(recycler);

        initRecyclerViewVertical(recyclerView);

        getMovieVertical(recyclerView, call);
    }

    //Shfaq movie details per filmat ne recycler view horizontale
    private void onClickListenerHorizontal(List<Movie> movieList) {
        movieAdapter = new MovieAdapter(movieList, new MovieClickListener() {
            @Override
            public void onMovieClick(Movie movie) {
                Movie.vibrateOnClick(getContext(), (short) 50);
                Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("movie", movie);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void onClickListenerVertical(List<Movie> movieList) {
        movieMenuAdapter = new MovieMenuAdapter(movieList, new MovieClickListener() {
            @Override
            public void onMovieClick(Movie movie) {
                Movie.vibrateOnClick(getContext(), (short) 50);
                Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("movie", movie);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    //Shfaq filmat horizontalisht ne recycler view
    private void getMovieHorizontal(final RecyclerView recyclerView, Call<MovieWrapper> call) {
        try {
            call.enqueue(new Callback<MovieWrapper>() {
                @Override
                public void onResponse(Call<MovieWrapper> call, Response<MovieWrapper> response) {
                    if (response.body() != null) {
                        List<Movie> movieList = response.body().getMovieList();

                        for (Movie m : movieList) {
                            m.mapGenres(genreList);
                        }

                        onClickListenerHorizontal(movieList);

                        recyclerView.setAdapter(movieAdapter);
                        recyclerView.smoothScrollToPosition(0);
                    }
                    else{
                        showError();
                    }
                    movieAdapter.notifyDataSetChanged();

                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<MovieWrapper> call, Throwable t) {
                        showError();
                }
            });
        }
        catch (Exception e) {
            showError();
        }
    }

    //Shfaq filmat vertikalisht ne recyclerview
    private void getMovieVertical(final RecyclerView recyclerView, Call<MovieWrapper> call) {
        try {
            call.enqueue(new Callback<MovieWrapper>() {
                @Override
                public void onResponse(Call<MovieWrapper> call, Response<MovieWrapper> response) {
                    if (response.body() != null) {
                        List<Movie> movieList = response.body().getMovieList();

                        for (Movie m : movieList) {
                            m.mapGenres(genreList);
                        }

                        onClickListenerVertical(movieList);

                        recyclerView.setAdapter(movieMenuAdapter);
                        recyclerView.smoothScrollToPosition(0);
                    }
                    else{
                        showError();
                    }
                    movieMenuAdapter.notifyDataSetChanged();

                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<MovieWrapper> call, Throwable t) {
                    showError();
                }
            });
        }
        catch (Exception e) {
            showError();
        }
    }

    private void getMovieGenres() {
        try {
            GetGenreDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetGenreDataService.class);
            Call<GenreWrapper> call = service.getGenre();
            call.enqueue(new Callback<GenreWrapper>() {
                @Override
                public void onResponse(Call<GenreWrapper> call, Response<GenreWrapper> response) {
                    if (response.body() != null) {
                        genreList = response.body().getGenres();
                    }
                }

                @Override
                public void onFailure(Call<GenreWrapper> call, Throwable t) {
                }
            });
        }
        catch (Exception e) {
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void showError() {
        Toast.makeText(getActivity(), "Check Wi-Fi", Toast.LENGTH_SHORT).show();
    }
}