package com.example.eclipse.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.MovieDetailActivity;
import com.example.eclipse.R;
import com.example.eclipse.adapter.FavoriteAdapter;
import com.example.eclipse.adapter.SearchAdapter;
import com.example.eclipse.data.FavoriteDbHelper;
import com.example.eclipse.listeners.MovieClickListener;
import com.example.eclipse.models.Genre;
import com.example.eclipse.models.Movie;

import java.util.ArrayList;
import java.util.List;

public class FavoritesFragment extends Fragment implements SearchView.OnQueryTextListener {

    private FavoriteDbHelper dbHelper;
    private FavoriteAdapter favoriteAdapter;

    private RecyclerView searchRecycler;
    private SearchAdapter searchAdapter;
    private List<Movie> movieList = new ArrayList<>();

    private SearchView searchView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        searchView = view.findViewById(R.id.search_favorite);
        searchView.setOnQueryTextListener(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getMovies();
    }

    private void getMovies() {
        searchRecycler = getActivity().findViewById(R.id.recycler_favorite);
        searchRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        searchRecycler.setItemAnimator(new DefaultItemAnimator());
        searchRecycler.addItemDecoration(new DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL));

        getFavorites();

        String movie = searchView.getQuery().toString();
        searchMovie(movie);

        searchRecycler.setAdapter(favoriteAdapter);
    }

    private void getFavorites() {
        movieList.clear();

        dbHelper = new FavoriteDbHelper(getContext());
        movieList = dbHelper.getMovies();

        populateMovies(movieList);
    }

    private void searchMovie(String searchMovie) {
        movieList.clear();

        dbHelper = new FavoriteDbHelper(getContext());
        movieList = dbHelper.searchMovie(searchMovie);

        populateMovies(movieList);
    }

    private void populateMovies(List<Movie> movies) {
        favoriteAdapter = new FavoriteAdapter(movies, new MovieClickListener() {
            @Override
            public void onMovieClick(Movie movie) {
                Movie.vibrateOnClick(getContext(), (short) 50);
                Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("movie", movie);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        favoriteAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        getMovies();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
