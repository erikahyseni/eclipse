package com.example.eclipse.fragments;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.eclipse.MainActivity;
import com.example.eclipse.R;
import com.example.eclipse.models.Movie;

import java.io.PipedInputStream;

public class SettingsFragment extends Fragment {

    private Switch notificationSwitch;
    private Button btnNotification;
    private TextView txtAbout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        notificationSwitch = view.findViewById(R.id.swNotification);

        txtAbout = view.findViewById(R.id.txtAbout);

        txtAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                AboutFragment aboutFragment = new AboutFragment();
                ft.replace(R.id.settingsFragment, new AboutFragment());
                ft.commit();
            }
        });

        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    Log.v("TAG", "Checked");
                    String title = "ECLIPSE";
                    String message = "Brand new movies coming up!";
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(),
                            "personal notifications")
                            .setSmallIcon(R.drawable.ic_baseline_notifications_24)
                            .setColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark))
                            .setContentTitle(title)
                            .setContentText(message)
                            .setVibrate(new long[] {1000, 1000, 1000, 1000, 1000})
                            .setLights(Color.BLUE, 3000, 3000)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                    Intent intent = new Intent(getContext(), MainActivity.class);

                    PendingIntent pendingIntent = PendingIntent.getActivity(getContext(),
                            0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(pendingIntent);

                    NotificationManager notificationManager = (NotificationManager) requireActivity()
                            .getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(100, builder.build());
                }
            }
        });
    }
}
