package com.example.eclipse.adapter;

import android.animation.ObjectAnimator;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.MovieDetailActivity;
import com.example.eclipse.R;
import com.example.eclipse.data.FavoriteDbHelper;
import com.example.eclipse.listeners.MovieClickListener;
import com.example.eclipse.models.Movie;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.logging.LogRecord;

public class MovieMenuAdapter extends RecyclerView.Adapter<MovieMenuAdapter.MyViewHolder> {

    private List<Movie> movieList;
    private MovieClickListener movieClickListener;

    public MovieMenuAdapter(List<Movie> movieList, MovieClickListener movieClickListener) {
        this.movieList = movieList;
        this.movieClickListener = movieClickListener;
    }

    @NonNull
    @Override
    public MovieMenuAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie_menu, parent, false);
        return new MovieMenuAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieMenuAdapter.MyViewHolder holder, int position) {
        Movie movie = movieList.get(position);

        holder.bind(movie, movieClickListener);
    }

    @Override
    public int getItemCount() {
        return null != movieList ? movieList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagePoster;
        private TextView txtTitle, txtRating, txtReleaseDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imagePoster = itemView.findViewById(R.id.movie_img_menu);
            txtTitle = itemView.findViewById(R.id.txtMovieMenuSmall);
            txtRating = itemView.findViewById(R.id.txtRatingMovieMenu);
            txtReleaseDate = itemView.findViewById(R.id.txtReleaseDateMenu);
        }

        public void bind(final Movie movie, final MovieClickListener movieClickListener) {
            final String posterPath = "https://image.tmdb.org/t/p/w342" + movie.getThumbnail();

            txtTitle.setText(movie.getTitle());
            txtReleaseDate.setText(movie.getReleaseDate());

            String vote = movie.getVoteAverage() + "/10";
            txtRating.setText(vote);

            Picasso.get()
                    .load(posterPath)
                    .placeholder(R.drawable.ic_baseline_cached_24)
                    .error(R.drawable.ic_baseline_block_24)
                    .into(imagePoster);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Movie.vibrateOnClick(v.getContext(), (short) 50);
                    movieClickListener.onMovieClick(movie);
                }
            });
        }
    }
}
