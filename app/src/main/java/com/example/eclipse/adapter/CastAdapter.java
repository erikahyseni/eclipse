package com.example.eclipse.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.R;
import com.example.eclipse.listeners.CastClickListener;
import com.example.eclipse.models.Cast;
import com.example.eclipse.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.MyViewHolder> {

    private List<Cast> castList;
    private CastClickListener castClickListener;

    public CastAdapter(List<Cast> castList, CastClickListener castClickListener) {
        this.castList = castList;
        this.castClickListener = castClickListener;
    }

    public CastAdapter(List<Cast> castList) {
        this.castList = castList;
    }

    @NonNull
    @Override
    public CastAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cast, parent, false);
        return new CastAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastAdapter.MyViewHolder holder, int position) {
        Cast cast = castList.get(position);

        holder.bind(cast, castClickListener);
    }

    @Override
    public int getItemCount() {
        return null != castList ? castList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageCast;
        private TextView txtCast, txtCharacter;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtCast = itemView.findViewById(R.id.txtCastName);
            txtCharacter = itemView.findViewById(R.id.txtCharacter);
            imageCast = itemView.findViewById(R.id.image_cast);
        }

        public void bind(final Cast cast, final CastClickListener castClickListener) {
            String posterPath = "https://image.tmdb.org/t/p/w342" + cast.getImage();

            txtCharacter.setText(cast.getCharacter());
            txtCast.setText(cast.getName());

            Picasso.get()
                    .load(posterPath)
                    .placeholder(R.drawable.ic_baseline_cached_24)
                    .error(R.drawable.ic_baseline_block_24)
                    .into(imageCast);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Movie.vibrateOnClick(itemView.getContext(), (short) 50);
                    castClickListener.onCastClick(cast);
                }
            });
        }
    }
}
