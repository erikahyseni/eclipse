package com.example.eclipse.adapter;

import android.animation.ObjectAnimator;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.R;
import com.example.eclipse.data.FavoriteDbHelper;
import com.example.eclipse.listeners.MovieClickListener;
import com.example.eclipse.models.Movie;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MyViewHolder> {

    private List<Movie> movieList;
    private MovieClickListener movieClickListener;

    public FavoriteAdapter(List<Movie> movieList, MovieClickListener movieClickListener) {
        this.movieList = movieList;
        this.movieClickListener = movieClickListener;
    }

    @NonNull
    @Override
    public FavoriteAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite, parent, false);
        return new FavoriteAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteAdapter.MyViewHolder holder, int position) {
        Movie movie = movieList.get(position);

        holder.bind(movie, movieClickListener);
    }

    @Override
    public int getItemCount() {
        return null != movieList ? movieList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagePoster;
        private TextView txtTitle, txtRating, txtReleaseDate;
        private FloatingActionButton favoriteButton;

        private FavoriteDbHelper dbHelper;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imagePoster = itemView.findViewById(R.id.movie_imgFav);
            txtTitle = itemView.findViewById(R.id.txtMovieSmallFav);
            txtRating = itemView.findViewById(R.id.txtRatingFav);
            txtReleaseDate = itemView.findViewById(R.id.txtReleaseDateFav);
            favoriteButton = itemView.findViewById(R.id.float_btn_fav_Fav);
        }

        public void bind(final Movie movie, final MovieClickListener movieClickListener){
            String posterPath = "https://image.tmdb.org/t/p/w342" + movie.getThumbnail();

            txtTitle.setText(movie.getTitle());
            txtReleaseDate.setText(movie.getReleaseDate());

            String vote = movie.getVoteAverage() + "/10";
            txtRating.setText(vote);

            Picasso.get()
                    .load(posterPath)
                    .placeholder(R.drawable.ic_baseline_cached_24)
                    .error(R.drawable.ic_baseline_block_24)
                    .into(imagePoster);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    movieClickListener.onMovieClick(movie);
                }
            });

            favoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Movie.vibrateOnClick(v.getContext(), (short) 50);
                    ObjectAnimator.ofFloat(favoriteButton, "rotation", 0f, 360f).setDuration(800).start();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            favoriteButton.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.ic_baseline_bookmark_border_24));
                            dbHelper = new FavoriteDbHelper(v.getContext());
                            dbHelper.deleteFavorite(movie.getId());

                            Snackbar.make(v, movie.getTitle() + " removed to favorites!", Snackbar.LENGTH_SHORT).show();
                        }
                    }, 400);
                }
            });
        }
    }
}
