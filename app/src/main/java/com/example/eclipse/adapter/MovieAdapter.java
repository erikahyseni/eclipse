package com.example.eclipse.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.R;
import com.example.eclipse.listeners.MovieClickListener;
import com.example.eclipse.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    private List<Movie> movieList;
    private MovieClickListener movieClickListener;

    public MovieAdapter(List<Movie> movieList, MovieClickListener movieClickListener) {
        this.movieList = movieList;
        this.movieClickListener = movieClickListener;
    }

    public MovieAdapter(List<Movie> movieList) {
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MyViewHolder holder, int position) {
        Movie movie = movieList.get(position);

        holder.bind(movie, movieClickListener);
    }

    @Override
    public int getItemCount() {
        return null != movieList ? movieList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagePoster;
        private TextView txtPoster;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imagePoster = itemView.findViewById(R.id.imageView);
            txtPoster = itemView.findViewById(R.id.txtMovieTitleMenuBig);
        }

        public void bind(final Movie movie, final MovieClickListener movieClickListener){
            String posterPath = "https://image.tmdb.org/t/p/w342" + movie.getThumbnail();

            txtPoster.setText(movie.getTitle());

            Picasso.get()
                    .load(posterPath)
                    .placeholder(R.drawable.ic_baseline_cached_24)
                    .error(R.drawable.ic_baseline_block_24)
                    .into(imagePoster);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Movie.vibrateOnClick(v.getContext(), (short) 50);
                    movieClickListener.onMovieClick(movie);
                }
            });
        }
    }
}
