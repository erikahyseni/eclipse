package com.example.eclipse.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eclipse.R;
import com.example.eclipse.listeners.TrailerClickListener;
import com.example.eclipse.models.Movie;
import com.example.eclipse.models.MovieTrailer;

import java.util.List;

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.MyViewHolder> {

    private final TrailerClickListener trailerClickListener;
    private final List<MovieTrailer> movieTrailerList;

    public TrailerAdapter(List<MovieTrailer> movieTrailerList, TrailerClickListener trailerClickListener) {
        this.movieTrailerList = movieTrailerList;
        this.trailerClickListener = trailerClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trailer, parent, false);
        return new TrailerAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        MovieTrailer movieTrailer = this.movieTrailerList.get(position);
        holder.bind(movieTrailer, trailerClickListener);
    }

    @Override
    public int getItemCount() {
        return null != this.movieTrailerList ? this.movieTrailerList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView movieTrailerName;
        private CardView cardView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            movieTrailerName = itemView.findViewById(R.id.txt_MovieTrailer);
            cardView = itemView.findViewById(R.id.cv_movie_trailer);
        }

        public void bind(final MovieTrailer movieTrailer, final TrailerClickListener trailerClickListener) {
            movieTrailerName.setText(movieTrailer.getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Movie.vibrateOnClick(v.getContext(), (short) 50);
                    trailerClickListener.onTrailerClick(movieTrailer);
                }
            });
        }
    }
}
