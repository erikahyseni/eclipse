package com.example.eclipse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eclipse.adapter.CastAdapter;
import com.example.eclipse.adapter.MovieAdapter;
import com.example.eclipse.adapter.SimilarAdapter;
import com.example.eclipse.adapter.TrailerAdapter;
import com.example.eclipse.clients.GetCastDataService;
import com.example.eclipse.clients.GetGenreDataService;
import com.example.eclipse.clients.GetMovieDataService;
import com.example.eclipse.clients.GetMovieTrailerService;
import com.example.eclipse.data.FavoriteDbHelper;
import com.example.eclipse.listeners.CastClickListener;
import com.example.eclipse.listeners.MovieClickListener;
import com.example.eclipse.listeners.TrailerClickListener;
import com.example.eclipse.models.Cast;
import com.example.eclipse.models.Genre;
import com.example.eclipse.models.Movie;
import com.example.eclipse.models.MovieTrailer;
import com.example.eclipse.utils.RetrofitClientInstance;
import com.example.eclipse.wrappers.CastWrapper;
import com.example.eclipse.wrappers.GenreWrapper;
import com.example.eclipse.wrappers.MovieTrailerWrapper;
import com.example.eclipse.wrappers.MovieWrapper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailActivity extends AppCompatActivity {

    private List<Genre> genreList = new ArrayList<>();
    private List<Cast> castList;
    private List<MovieTrailer> trailerList;

    private TextView txtTitle, txtReleaseDate, txtInfo, txtRating, txtGenres, txtTrailer;
    private ImageView imgCover, imgPoster;
    private FloatingActionButton favoriteButton;

    private String movieTitle, movieReleaseDate, movieInfo, moviePosterPath, movieCoverPath;
    private int movieId;
    private double movieRating;
    private RecyclerView recycler_cast, recycler_similar;

    private MovieAdapter movieAdapter;
    private CastAdapter castAdapter;
    private SimilarAdapter similarAdapter;
    private TrailerAdapter trailerAdapter;

    private FavoriteDbHelper dbHelper;

    private Movie movie;
    private Cast cast;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        getMovieGenres();

        recycler_cast = findViewById(R.id.recycler_cast);
        recycler_cast.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));

        recycler_similar = findViewById(R.id.recycler_similar);
        recycler_similar.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));

        txtTitle = findViewById(R.id.txtMovieDetail);
        txtRating = findViewById(R.id.txtRating);
        txtReleaseDate = findViewById(R.id.txtReleaseDate);
        txtInfo = findViewById(R.id.txtInfo);
        txtGenres = findViewById(R.id.txtGenre);
        txtTrailer = findViewById(R.id.txtWatchTrailer);

        favoriteButton = findViewById(R.id.fav_button);

        imgPoster = findViewById(R.id.detail_movie_image);
        imgCover = findViewById(R.id.detail_movie_cover);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                movie = (Movie) bundle.getSerializable("movie");
                if (movie != null) {
                    dbHelper = new FavoriteDbHelper(this);
                    if (dbHelper.checkIfMovieExist(movie.getId())) {
                        favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_bookmark_24));
                    }
                    else {
                        favoriteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_bookmark_border_24));
                    }
                    populateMovieDetail(movie);
                    if (isNetworkAvailable()) {
                        getCast(movie.getId());
                        getSimilarMovies(movie.getId());
                    }
                }
            }
        }

        txtTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Movie.vibrateOnClick(v.getContext(), (short) 50);
                Intent intent = new Intent(getApplicationContext(), TrailerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("movie", movie);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Movie.vibrateOnClick(v.getContext(), (short) 50);
                ObjectAnimator.ofFloat(favoriteButton, "rotation", 0f, 360f).setDuration(800).start();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (savedInstanceState == null) {
                            Intent intent = getIntent();
                            Bundle bundle = intent.getExtras();
                            if (bundle != null) {
                                dbHelper = new FavoriteDbHelper(MovieDetailActivity.this);
                                movie = (Movie) bundle.getSerializable("movie");
                                if (movie != null) {
                                    if (!dbHelper.checkIfMovieExist(movie.getId())) {
                                        favoriteButton.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.ic_baseline_bookmark_24));
                                        saveFavorite();
                                        Snackbar.make(v, movie.getTitle() + " added to favorites!", Snackbar.LENGTH_SHORT).show();
                                    }
                                    else {
                                        favoriteButton.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.ic_baseline_bookmark_border_24));

                                        dbHelper = new FavoriteDbHelper(MovieDetailActivity.this);
                                        dbHelper.deleteFavorite(movie.getId());

                                        Snackbar.make(v, movie.getTitle() + " removed to favorites!", Snackbar.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, 400);
            }
        });
    }

    private void populateMovieDetail(Movie m) {
        movieId = m.getId();
        movieTitle = m.getTitle();
        movieInfo = m.getDescription();
        movieRating = m.getVoteAverage();
        movieReleaseDate = m.getReleaseDate();
        movieCoverPath = m.getCover_image();
        moviePosterPath = m.getThumbnail();

        String poster = "https://image.tmdb.org/t/p/w342" + moviePosterPath;
        String cover = "https://image.tmdb.org/t/p/w1280" + movieCoverPath;

        Picasso.get()
                .load(poster)
                .into(imgPoster);

        Picasso.get()
                .load(cover)
                .into(imgCover);

        List<String> genreString = new ArrayList<>();
        if (m.getGenre() != null) {
            for (Genre genre : movie.getGenreList()) {
                genreString.add(genre.getGenres());
            }
        }
        String genreRe = genreString.toString().replace("[", "");
        genreRe = genreRe.replace("]", "");
        txtGenres.setText(genreRe);

        txtTitle.setText(movieTitle);
        txtInfo.setText(movieInfo);
        txtReleaseDate.setText(movieReleaseDate);

        String vote = movieRating + "/10";
        txtRating.setText(vote);
    }

    private void saveFavorite() {
        dbHelper = new FavoriteDbHelper(MovieDetailActivity.this);
        movie = new Movie();

        String posterPath = moviePosterPath.replace("https://image.tmdb.org/t/p/w342" , "");
        String coverPath = movieCoverPath.replace("https://image.tmdb.org/t/p/w1280" , "");

        movie.setId(movieId);
        movie.setTitle(movieTitle);
        movie.setVoteAverage(movieRating);
        movie.setThumbnail(posterPath);
        movie.setCover_image(coverPath);
        movie.setDescription(movieInfo);
        movie.setReleaseDate(movieReleaseDate);

        dbHelper.addFavorite(movie);
    }

    private void getMovieGenres() {
        try {
            GetGenreDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetGenreDataService.class);
            Call<GenreWrapper> call = service.getGenre();
            call.enqueue(new Callback<GenreWrapper>() {
                @Override
                public void onResponse(Call<GenreWrapper> call, Response<GenreWrapper> response) {
                    if (response.body() != null) {
                        genreList = response.body().getGenres();
                    }
                }

                @Override
                public void onFailure(Call<GenreWrapper> call, Throwable t) {
                }
            });
        }
        catch (Exception e) {
        }
    }

    private void populateCast(List<Cast> casts) {
        if (casts.size() > 0) {
            castAdapter = new CastAdapter(casts, new CastClickListener() {
                @Override
                public void onCastClick(Cast cast) {
                    Movie.vibrateOnClick(MovieDetailActivity.this, (short) 50);
                    Intent intent = new Intent(MovieDetailActivity.this, MovieActorActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("cast", cast);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
            recycler_cast.setAdapter(castAdapter);
        }
    }

    private void getCast(int id) {
        GetCastDataService castService = RetrofitClientInstance.getRetrofitInstance().create(GetCastDataService.class);
        Call<CastWrapper> call = castService.getCast(id);
        call.enqueue(new Callback<CastWrapper>() {
            @Override
            public void onResponse(Call<CastWrapper> call, Response<CastWrapper> response) {
                if (response.body() != null) {
                    castList = response.body().getCastList();
                    populateCast(castList);
                }
            }

            @Override
            public void onFailure(Call<CastWrapper> call, Throwable t) {

            }
        });
    }

    private void populateSimilarMovies(List<Movie> movieList) {
        if (movieList.size() > 0) {
            similarAdapter = new SimilarAdapter(movieList, new MovieClickListener() {
                @Override
                public void onMovieClick(Movie movie) {
                    Movie.vibrateOnClick(MovieDetailActivity.this, (short) 50);
                    Intent intent = new Intent(MovieDetailActivity.this, MovieDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("movie", movie);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
            recycler_similar.setAdapter(similarAdapter);
        }
    }

    private void getSimilarMovies(int movieId) {
        try {
            GetMovieDataService movieService = RetrofitClientInstance.getRetrofitInstance().create(GetMovieDataService.class);
            Call<MovieWrapper> call = movieService.getSimilarMovie(movieId);

            call.enqueue(new Callback<MovieWrapper>() {
                @Override
                public void onResponse(Call<MovieWrapper> call, Response<MovieWrapper> response) {
                    if (response.body() != null) {
                        List<Movie> movieList = response.body().getMovieList();

                        for (Movie m : movieList) {
                            m.mapGenres(genreList);
                        }

                        populateSimilarMovies(movieList);
                    }
                }

                @Override
                public void onFailure(Call<MovieWrapper> call, Throwable t) {

                }
            });
        }
        catch (Exception e) {

        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("movie", movie);;
    }
}