package com.example.eclipse.models;

import com.google.gson.annotations.SerializedName;

public class Actor {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("birthday")
    private String birthDay;
    @SerializedName("deathday")
    private String deathDay;
    @SerializedName("profile_path")
    private String image;
    @SerializedName("imdb_id")
    private String imdb;
    @SerializedName("biography")
    private String bio;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getDeathDay() {
        return deathDay;
    }

    public void setDeathDay(String deathDay) {
        this.deathDay = deathDay;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
