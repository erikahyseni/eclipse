package com.example.eclipse.clients;

import com.example.eclipse.wrappers.GenreWrapper;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetGenreDataService {
    @GET("genre/movie/list?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US")
    Call<GenreWrapper> getGenre();
}
