package com.example.eclipse.clients;

import com.example.eclipse.wrappers.MovieWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetMovieDataService {

    @GET("movie/popular?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US&page=1&region=US")
    Call<MovieWrapper> moviePopular();

    @GET("movie/top_rated?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US&page=1&region=US")
    Call<MovieWrapper> movieTopRated();

    @GET("movie/upcoming?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US&page=1&region=US")
    Call<MovieWrapper> movieUpcoming();

    @GET("movie/now_playing?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US&page=1&region=US")
    Call<MovieWrapper> movieNowPlaying();

    @GET("movie/{id}/similar?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US&page=1")
    Call<MovieWrapper> getSimilarMovie(@Path("id") int movieId);

    @GET("search/movie?api_key=2f002b3dd7595a2539855eac9cb0e27d")
    Call<MovieWrapper> getMovie(@Query("query") String movieQuery);
}
