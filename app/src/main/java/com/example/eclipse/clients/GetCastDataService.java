package com.example.eclipse.clients;

import com.example.eclipse.models.Actor;
import com.example.eclipse.wrappers.ActorWrapper;
import com.example.eclipse.wrappers.CastWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetCastDataService {
    @GET("movie/{id}/credits?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US")
    Call<CastWrapper> getCast(@Path("id") int movieId);

    @GET("person/{id}?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US")
    Call<Actor> getActor(@Path("id") int castId);
}
