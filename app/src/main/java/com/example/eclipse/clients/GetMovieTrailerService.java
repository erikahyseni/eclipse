package com.example.eclipse.clients;

import com.example.eclipse.wrappers.MovieTrailerWrapper;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetMovieTrailerService {
    @GET("movie/{id}/videos?api_key=2f002b3dd7595a2539855eac9cb0e27d&language=en-US")
    Call<MovieTrailerWrapper> getTrailers(@Path("id") int movieId);
}
